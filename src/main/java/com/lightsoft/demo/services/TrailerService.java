package com.lightsoft.demo.services;

import com.lightsoft.demo.domain.models.dto.TrailerDTO;

import java.util.Set;

public interface TrailerService {
    TrailerDTO addTrailer(TrailerDTO trailerServiceModel);

    Set<TrailerDTO> getAllTrailer();

    TrailerDTO editTrailer(TrailerDTO trailerServiceModel);

    void deleteTrailer(Long id);

    TrailerDTO findById(Long id);
}
