package com.lightsoft.demo.services;

import com.lightsoft.demo.domain.models.dto.CompanyDTO;

import java.util.Set;

public interface CompanyService {
    Set<CompanyDTO> getAllCompanies();

    CompanyDTO addCompany(CompanyDTO companyServiceModel);

    CompanyDTO editCompany(CompanyDTO companyServiceModel);

    void deleteCompany(Long id);

    CompanyDTO addTruckToCompany(Long id, Long truckId);

    CompanyDTO removeTruckFromCompany(Long id, Long truckId);

    CompanyDTO findById(Long id);
}
