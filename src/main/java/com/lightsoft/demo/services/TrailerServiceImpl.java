package com.lightsoft.demo.services;

import com.lightsoft.demo.domain.entities.TrailerEntity;
import com.lightsoft.demo.domain.models.dto.TrailerDTO;
import com.lightsoft.demo.domain.repository.TrailerRepository;
import com.lightsoft.demo.domain.repository.TruckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TrailerServiceImpl implements TrailerService {
    private final TrailerRepository trailerRepository;
    private final TruckRepository truckRepository;

    @Autowired
    public TrailerServiceImpl(TrailerRepository trailerRepository, TruckRepository truckRepository) {
        this.trailerRepository = trailerRepository;
        this.truckRepository = truckRepository;
    }

    @Override
    public Set<TrailerDTO> getAllTrailer() {
        List<TrailerEntity> trailerEntities = this.trailerRepository.findAll();
        Set<TrailerDTO> trailerDTOS = new HashSet<>();
        for (TrailerEntity trailerEntity : trailerEntities) {
            TrailerDTO trailerDTO = trailerEntity.toDto();
            trailerDTOS.add(trailerDTO);
        }
        return trailerDTOS;
    }

    @Override
    public TrailerDTO findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Id can not be null!");
        }

        TrailerEntity trailer = this.trailerRepository.findById(id).orElse(null);
        if (trailer != null) {
            TrailerDTO returnedDto = trailer.toDto();
            return returnedDto;
        } else {
            throw new IllegalArgumentException("Trailer does not exist!");
        }
    }

    @Override
    public TrailerDTO addTrailer(TrailerDTO trailerDTO) {

        if (trailerDTO.getId() != null) {
            throw new IllegalArgumentException("You haven't to enter id!");
        } else {
            TrailerEntity trailer = new TrailerEntity();
            trailer.toEntity(trailerDTO);
            TrailerEntity savedEntity = this.trailerRepository.save(trailer);
            TrailerDTO savedDto = savedEntity.toDto();
            return savedDto;
        }
    }


    @Override
    public TrailerDTO editTrailer(TrailerDTO trailerDTO) {
        TrailerEntity trailer = this.trailerRepository.findById(trailerDTO.getId()).orElse(null);
        if (trailer != null) {
            trailer.toEntity(trailerDTO);
            TrailerEntity savedEntity = this.trailerRepository.saveAndFlush(trailer);
            TrailerDTO savedDto = savedEntity.toDto();
            return savedDto;
        }
        throw new IllegalArgumentException("Trailer does not exist");
    }

    @Override
    public void deleteTrailer(Long id) {
        TrailerEntity trailerEntity = this.trailerRepository.findById(id).orElse(null);
        if (trailerEntity != null) {
            this.trailerRepository.delete(trailerEntity);
        } else {
            throw new IllegalArgumentException("Trailer does not exist!");
        }
    }

}
