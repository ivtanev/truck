package com.lightsoft.demo.services;

import com.lightsoft.demo.domain.models.dto.TruckDTO;

import java.util.Set;

public interface TruckService {
    TruckDTO addTruck(TruckDTO truckServiceModel);

    Set<TruckDTO> getAllTruck();

    TruckDTO editTruck(TruckDTO truckServiceModel);

    void deleteTruck(Long id);

    TruckDTO addTrailerToTruck(Long id, Long trailerId);

    TruckDTO removeTrailerFromTruck(Long id, Long trailerId);

    TruckDTO findById(Long id);
}
