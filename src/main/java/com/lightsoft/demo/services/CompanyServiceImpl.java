package com.lightsoft.demo.services;

import com.lightsoft.demo.domain.entities.CompanyEntity;
import com.lightsoft.demo.domain.entities.TruckEntity;
import com.lightsoft.demo.domain.models.dto.CompanyDTO;
import com.lightsoft.demo.domain.repository.CompanyRepository;
import com.lightsoft.demo.domain.repository.TruckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CompanyServiceImpl implements CompanyService {
    private final CompanyRepository companyRepository;
    private final TruckRepository truckRepository;

    @Autowired
    public CompanyServiceImpl(CompanyRepository companyRepository, TruckRepository truckRepository) {
        this.companyRepository = companyRepository;
        this.truckRepository = truckRepository;
    }

    @Override
    public Set<CompanyDTO> getAllCompanies() {
        List<CompanyEntity> companies = this.companyRepository.findAll();
        Set<CompanyDTO> companyDTOS = new HashSet<>();
        for (CompanyEntity company : companies) {
            CompanyDTO companyDTO = company.toDto();
            companyDTOS.add(companyDTO);
        }
        return companyDTOS;
    }

    @Override
    public CompanyDTO addCompany(CompanyDTO companyDTO) {

        if (!this.companyRepository.findByName(companyDTO.getName()).isPresent()) {
            CompanyEntity company = new CompanyEntity();
            company.toEntity(companyDTO);
            CompanyEntity savedEntity = this.companyRepository.saveAndFlush(company);
            return savedEntity.toDto();
        } else {
            throw new IllegalArgumentException("Company with this name already exist!");
        }

    }

    @Override
    public CompanyDTO findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Id can not be null!");
        }

        CompanyEntity company = this.companyRepository.findById(id).orElse(null);
        if (company != null) {
            CompanyDTO returnedDto = company.toDto();
            return returnedDto;
        } else {
            throw new IllegalArgumentException("Company does not exist!");
        }
    }

    @Override
    public CompanyDTO editCompany(CompanyDTO companyDTO) {
        CompanyEntity company = this.companyRepository.findById(companyDTO.getId()).orElse(null);
        if (company != null) {
            company.toEntity(companyDTO);
            CompanyEntity savedEntity = this.companyRepository.saveAndFlush(company);
            CompanyDTO savedDto = savedEntity.toDto();
            return savedDto;
        }
        throw new IllegalArgumentException("Company does not exist!");
    }

    @Override
    public void deleteCompany(Long id) {
        CompanyEntity company = this.companyRepository.findById(id).orElse(null);
        if (company != null) {
            this.companyRepository.delete(company);
        } else {
            throw new IllegalArgumentException("Company does not exist!");
        }
    }

    @Override
    public CompanyDTO addTruckToCompany(Long id, Long truckId) {
        CompanyEntity company = this.companyRepository.findById(id).orElse(null);
        TruckEntity truck = this.truckRepository.findById(truckId).orElse(null);
        if (company != null && truck != null) {
            company.getTrucks().add(truck);
            CompanyEntity savedEntity = this.companyRepository.save(company);
            CompanyDTO savedDto = savedEntity.toDto();
            return savedDto;
        } else {
            if (company == null) {
                throw new IllegalArgumentException("Company does not exist!");
            }
            throw new IllegalArgumentException("Truck does not exist!");
        }
    }

    @Override
    public CompanyDTO removeTruckFromCompany(Long id, Long truckId) {
        CompanyEntity company = this.companyRepository.findById(id).orElse(null);
        TruckEntity truck = this.truckRepository.findById(truckId).orElse(null);
        if (company != null && truck != null) {
            company.getTrucks().remove(truck);
//            company.setTrucks(company.getTrucks()
//                    .stream()
//                    .filter(t -> !t.getId().equals(truck.getId()))
//                    .collect(Collectors.toSet()));

            CompanyEntity savedEntity = this.companyRepository.saveAndFlush(company);
            CompanyDTO savedDto = savedEntity.toDto();
            return savedDto;
        } else {
            throw new IllegalArgumentException("Company or Truck does not exist!");
        }
    }
}
