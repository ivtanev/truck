package com.lightsoft.demo.services;

import com.lightsoft.demo.domain.entities.TrailerEntity;
import com.lightsoft.demo.domain.entities.TruckEntity;
import com.lightsoft.demo.domain.models.dto.TrailerDTO;
import com.lightsoft.demo.domain.models.dto.TruckDTO;
import com.lightsoft.demo.domain.repository.CompanyRepository;
import com.lightsoft.demo.domain.repository.TrailerRepository;
import com.lightsoft.demo.domain.repository.TruckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TruckServiceImpl implements TruckService {
    private final TruckRepository truckRepository;
    private final TrailerRepository trailerRepository;
    private final CompanyRepository companyRepository;

    @Autowired
    public TruckServiceImpl(TruckRepository truckRepository, TrailerRepository trailerRepository, CompanyRepository companyRepository) {
        this.truckRepository = truckRepository;
        this.trailerRepository = trailerRepository;
        this.companyRepository = companyRepository;
    }

    @Override
    public Set<TruckDTO> getAllTruck() {
        List<TruckEntity> trucks = this.truckRepository.findAll();
        Set<TruckDTO> truckDTOS = new HashSet<>();

        for (TruckEntity truck : trucks) {
            TruckDTO truckDTO = truck.toDto();
            truckDTOS.add(truckDTO);
        }
        return truckDTOS;
    }

    @Override
    public TruckDTO findById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Id can not be null!");
        }

        TruckEntity company = this.truckRepository.findById(id).orElse(null);
        if (company != null) {
            TruckDTO returnedDto = company.toDto();
            return returnedDto;
        } else {
            throw new IllegalArgumentException("Company does not exist!");
        }
    }

    @Override
    public TruckDTO addTruck(TruckDTO truckDTO) {

        if (truckDTO.getId() != null) {
            throw new IllegalArgumentException("You haven't to enter id");
        } else {
            TruckEntity truck = new TruckEntity();
            truck.toEntity(truckDTO);
            TruckEntity savedEntity = this.truckRepository.saveAndFlush(truck);
            TruckDTO savedDto = savedEntity.toDto();
            return savedDto;
        }
    }

    @Override
    public TruckDTO editTruck(TruckDTO truckDTO) {
        TruckEntity truck = this.truckRepository.findById(truckDTO.getId()).orElse(null);
        if (truck != null) {
            truck.toEntity(truckDTO);
            TruckEntity savedEntity = this.truckRepository.saveAndFlush(truck);
            TruckDTO savedDto = savedEntity.toDto();
            return savedDto;
        } else {
            throw new IllegalArgumentException("Truck does not exist!");
        }
    }


    @Override
    public void deleteTruck(Long id) {
        TruckEntity truck = this.truckRepository.findById(id).orElse(null);
        if (truck != null) {
            this.truckRepository.delete(truck);
        } else {
            throw new IllegalArgumentException("Truck does not exist!");
        }
    }

    @Override
    public TruckDTO addTrailerToTruck(Long id, Long trailerId){
        TruckEntity truck = this.truckRepository.findById(id).orElse(null);
        TrailerEntity trailer = this.trailerRepository.findById(trailerId).orElse(null);
        if (truck != null && trailer != null) {
            List<TruckEntity> truckEntities = this.truckRepository.findAll();
            for (TruckEntity truckEntity : truckEntities) {
                if (truckEntity.getTrailer() != null) {
                    if (truckEntity.getTrailer().getId().equals(trailerId)) {
                        truckEntity.setTrailer(null);
                        break;
                    }
                }
            }
            truck.setTrailer(trailer);
            TruckEntity savedEntity = this.truckRepository.save(truck);
            TruckDTO savedDto = savedEntity.toDto();
            return savedDto;
        } else {
            if (truck == null) {
                throw new IllegalArgumentException("Truck  does not exist!");
            }
            throw new IllegalArgumentException("Trailer does not exist!");
        }
    }

    @Override
    public TruckDTO removeTrailerFromTruck(Long id, Long trailerId) {
        TruckEntity truck = this.truckRepository.findById(id).orElse(null);
        TrailerEntity trailer = this.trailerRepository.findById(trailerId).orElse(null);

        if (truck != null && trailer != null) {
            truck.setTrailer(null);
            TruckEntity savedEntity = this.truckRepository.saveAndFlush(truck);
            TruckDTO savedDto = savedEntity.toDto();
            return savedDto;
        } else {
            if (truck == null) {
                throw new IllegalArgumentException("Truck does not exist!");
            }
            throw new IllegalArgumentException("Trailer does not exist!");
        }
    }

}
