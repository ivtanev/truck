package com.lightsoft.demo.domain.repository;

import com.lightsoft.demo.domain.entities.TrailerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrailerRepository extends JpaRepository<TrailerEntity, Long> {

}
