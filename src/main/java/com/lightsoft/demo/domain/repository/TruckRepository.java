package com.lightsoft.demo.domain.repository;

import com.lightsoft.demo.domain.entities.TruckEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TruckRepository extends JpaRepository<TruckEntity, Long> {

    Optional<TruckEntity> findByModel(String model);

    @Override
    Optional<TruckEntity> findById(Long id);
}
