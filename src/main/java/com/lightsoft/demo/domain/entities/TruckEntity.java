package com.lightsoft.demo.domain.entities;

import com.lightsoft.demo.domain.models.dto.TrailerDTO;
import com.lightsoft.demo.domain.models.dto.TruckDTO;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "trucks")
public class TruckEntity extends BaseEntity<TruckDTO> {
    private String model;
    private TrailerEntity trailer;
    private Set<CompanyEntity> companies;


    @Column(name = "model", unique = true)
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @OneToOne(targetEntity = TrailerEntity.class, fetch = FetchType.EAGER)
    @JoinColumn(name = "trailer_id")
    public TrailerEntity getTrailer() {
        return trailer;
    }

    public void setTrailer(TrailerEntity trailer) {
        this.trailer = trailer;
    }

    @ManyToMany(mappedBy = "trucks")
    public Set<CompanyEntity> getCompanies() {
        return companies;
    }

    public void setCompanies(Set<CompanyEntity> companies) {
        this.companies = companies;
    }

    @Override
    public TruckDTO toDto(){
        TruckDTO truckDTO = super.toDto();
        truckDTO.setModel(this.getModel());
        if (this.getCompanies() != null) {
            this.getCompanies().forEach(entity -> truckDTO.getCompanies().add(entity.getId()));
        }

        if (this.getTrailer() != null) {
            truckDTO.setTrailer(this.getTrailer().toDto());
        }
        return truckDTO;
    }

    @Override
    public void toEntity(TruckDTO dto) {
        super.toEntity(dto);
        this.setModel(dto.getModel());

    }
}
