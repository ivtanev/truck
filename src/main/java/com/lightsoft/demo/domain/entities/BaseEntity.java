package com.lightsoft.demo.domain.entities;


import com.lightsoft.demo.domain.models.dto.BaseDto;

import javax.persistence.*;
import java.lang.reflect.ParameterizedType;

@MappedSuperclass
public abstract class BaseEntity<T extends BaseDto> implements Convertable<T> {
    private Long id;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @Basic(optional = false)
    @SequenceGenerator(name = "seq", sequenceName = "shipwreck_seq", allocationSize = 1)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public T toDto() {
        try {
            Object object = ((Class) ((ParameterizedType) this.getClass().
                    getGenericSuperclass()).getActualTypeArguments()[0]).newInstance();
            T dto = (T) object;
            dto.setId(this.getId());
            return dto;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void toEntity(T dto) {
        this.setId(dto.getId());
    }
}
