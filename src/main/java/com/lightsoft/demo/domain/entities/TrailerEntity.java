package com.lightsoft.demo.domain.entities;

import com.lightsoft.demo.domain.models.dto.TrailerDTO;
import com.lightsoft.demo.domain.models.dto.TruckDTO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "trailers")
public class TrailerEntity extends BaseEntity<TrailerDTO> {
    private String serialNumber;
    private TruckEntity truck;

    @Column(name = "serialNumber")
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @OneToOne(mappedBy = "trailer")
    public TruckEntity getTruck() {
        return truck;
    }

    public void setTruck(TruckEntity truck) {
        this.truck = truck;
    }

    @Override
    public TrailerDTO toDto(){
        TrailerDTO trailerDTO = super.toDto();
        trailerDTO.setSerialNumber(this.getSerialNumber());
        return trailerDTO;
//        super.toDto(dto);
//        dto.setSerialNumber(this.getSerialNumber());
//        return dto;
    }

    @Override
    public void toEntity(TrailerDTO dto) {
        super.toEntity(dto);
        this.setSerialNumber(dto.getSerialNumber());
    }
}
