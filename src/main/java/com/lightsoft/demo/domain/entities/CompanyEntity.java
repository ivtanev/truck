package com.lightsoft.demo.domain.entities;

import com.lightsoft.demo.domain.models.dto.CompanyDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "companies")
public class CompanyEntity extends BaseEntity<CompanyDTO> {

    private String name;
    private Set<TruckEntity> trucks = new HashSet<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @ManyToMany(targetEntity = TruckEntity.class)
    @JoinTable(
            name = "companies_trucks",
            joinColumns = @JoinColumn(name = "company_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "truck_id", referencedColumnName = "id")
    )
    public Set<TruckEntity> getTrucks() {
        return trucks;
    }

    public void setTrucks(Set<TruckEntity> trucks) {
        this.trucks = trucks;
    }

    @Override
    public CompanyDTO toDto(){
        CompanyDTO companyDTO = super.toDto();
        companyDTO.setName(this.getName());
        if(this.getTrucks() != null){
            companyDTO.setTrucks(this.getTrucks().stream().map(truckEntity -> {
               return truckEntity.toDto();
            }).collect(Collectors.toSet()));
        }
        return companyDTO;
//        super.toDto(dto);
//        dto.setName(this.getName());
//        if (this.getTrucks() != null) {
//            dto.setTrucks(this.getTrucks().stream().map(truckEntity -> {
//                return truckEntity.toDto(new TruckDTO());
//            }).collect(Collectors.toSet()));
//        }
//        return dto;
    }

    @Override
    public void toEntity(CompanyDTO dto) {
        super.toEntity(dto);
        this.setName(dto.getName());
    }

}
