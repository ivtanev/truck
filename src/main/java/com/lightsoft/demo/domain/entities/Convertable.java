package com.lightsoft.demo.domain.entities;

import com.lightsoft.demo.domain.models.dto.BaseDto;

import java.lang.reflect.InvocationTargetException;

public interface Convertable<T extends BaseDto> {
    abstract T toDto();
    void toEntity(T dto);
}
