package com.lightsoft.demo.domain.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TruckDTO extends BaseDto{

    private String model;
    private Set<Long> companies = new HashSet<>();
    private TrailerDTO trailer;


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Set<Long> getCompanies() {
        return companies;
    }

    public TrailerDTO getTrailer() {
        return trailer;
    }

    public void setTrailer(TrailerDTO trailer) {
        this.trailer = trailer;
    }
}
