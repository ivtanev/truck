package com.lightsoft.demo.domain.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TrailerDTO extends BaseDto{

    private String serialNumber;
    private TruckDTO truckDTO;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public TruckDTO getTruckDTO() {
        return truckDTO;
    }

    public void setTruckDTO(TruckDTO truckDTO) {
        this.truckDTO = truckDTO;
    }
}
