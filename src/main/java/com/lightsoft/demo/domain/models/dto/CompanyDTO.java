package com.lightsoft.demo.domain.models.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CompanyDTO extends BaseDto{
    private String name;
    private Set<TruckDTO> trucks;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<TruckDTO> getTrucks() {
        return trucks;
    }

    public void setTrucks(Set<TruckDTO> trucks) {
        this.trucks = trucks;
    }
}
