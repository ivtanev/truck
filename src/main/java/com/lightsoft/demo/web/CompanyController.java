package com.lightsoft.demo.web;

import com.lightsoft.demo.domain.models.dto.CompanyDTO;
import com.lightsoft.demo.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Set;

@RestController
public class CompanyController {

    private final CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/companies")
    public Set<CompanyDTO> findAllCompanies() {

        return Collections.unmodifiableSet(this.companyService.getAllCompanies());
    }

    @GetMapping("/companies/{id}")
    public CompanyDTO findById(@PathVariable Long id) {

        return this.companyService.findById(id);
    }

    @PostMapping("/companies/add")
    public CompanyDTO registerCompany(@RequestBody CompanyDTO companyDTO) {

        return companyService.addCompany(companyDTO);

    }

    @PostMapping("/companies/bind/{id}/{truckId}")
    public CompanyDTO addTruckToCompany(@PathVariable Long id, @PathVariable Long truckId) {

        return companyService.addTruckToCompany(id, truckId);
    }

    @PostMapping("/companies/unbind/{id}/{truckId}")
    public CompanyDTO removeTruckFromCompany(@PathVariable Long id, @PathVariable Long truckId) {

        return companyService.removeTruckFromCompany(id, truckId);
    }

    @PutMapping("/companies/edit")
    public CompanyDTO editCompany(@RequestBody CompanyDTO companyDTO) {

        return companyService.editCompany(companyDTO);
    }

    @DeleteMapping("/companies/delete/{id}")
    public void deleteCompany(@PathVariable Long id) {

        companyService.deleteCompany(id);
    }

}
