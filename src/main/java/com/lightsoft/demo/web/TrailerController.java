package com.lightsoft.demo.web;

import com.lightsoft.demo.domain.models.dto.TrailerDTO;
import com.lightsoft.demo.services.TrailerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Set;

@RestController
public class TrailerController {
    private final TrailerService trailerService;

    @Autowired
    public TrailerController(TrailerService trailerService) {
        this.trailerService = trailerService;
    }

    @GetMapping("/trailers")
    public Set<TrailerDTO> findAllTrailer() {

        return Collections.unmodifiableSet(this.trailerService.getAllTrailer());
    }

    @GetMapping("/trailers/{id}")
    public TrailerDTO findById(@PathVariable Long id) {

        return this.trailerService.findById(id);
    }

    @PostMapping("/trailers/add")
    public TrailerDTO registerTrailer(@RequestBody TrailerDTO trailerDTO) {

        return trailerService.addTrailer(trailerDTO);
    }

    @PutMapping("/trailers/edit")
    public TrailerDTO editTrailer(@RequestBody TrailerDTO trailerDTO) {

        return trailerService.editTrailer(trailerDTO);
    }

    @DeleteMapping("/trailers/delete/{id}")
    public void deleteTrailer(@PathVariable Long id) {
        trailerService.deleteTrailer(id);
    }
}
