package com.lightsoft.demo.web;

import com.lightsoft.demo.domain.models.dto.TruckDTO;
import com.lightsoft.demo.services.TruckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.Set;

@RestController
public class TruckController {
    private final TruckService truckService;

    @Autowired
    public TruckController(TruckService truckService) {
        this.truckService = truckService;
    }

    @GetMapping("/trucks")
    public Set<TruckDTO> findAllTrucks() {

        return Collections.unmodifiableSet(truckService.getAllTruck());
    }

    @GetMapping("/trucks/{id}")
    public TruckDTO findById(@PathVariable Long id) {
        return this.truckService.findById(id);
    }

    @PostMapping("/trucks/add")
    public TruckDTO registerTruck(@RequestBody TruckDTO truckDTO) {

        return this.truckService.addTruck(truckDTO);
    }

    @PutMapping("/trucks/edit")
    public TruckDTO editTruck(@RequestBody TruckDTO truckDTO) {

        return this.truckService.editTruck(truckDTO);
    }

    @DeleteMapping("/trucks/delete/{id}")
    public void deleteTruck(@PathVariable Long id) {

        truckService.deleteTruck(id);
    }

    @PostMapping("/trucks/bind/{id}/{trailerId}")
    public TruckDTO addTrailerToTruck(@PathVariable Long id, @PathVariable Long trailerId) {
        return truckService.addTrailerToTruck(id, trailerId);
    }

    @PostMapping("/trucks/unbind/{id}/{trailerId}")
    public TruckDTO removeTrailerFromTruck(@PathVariable Long id, @PathVariable Long trailerId) {
        return truckService.removeTrailerFromTruck(id, trailerId);
    }


}
