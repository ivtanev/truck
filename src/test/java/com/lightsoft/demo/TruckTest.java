package com.lightsoft.demo;

import com.lightsoft.demo.domain.entities.CompanyEntity;
import com.lightsoft.demo.domain.entities.TrailerEntity;
import com.lightsoft.demo.domain.entities.TruckEntity;
import com.lightsoft.demo.domain.repository.CompanyRepository;
import com.lightsoft.demo.domain.repository.TrailerRepository;
import com.lightsoft.demo.domain.repository.TruckRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class TruckTest {

    @Autowired
    private TruckRepository truckRepository;
    @Autowired
    private TrailerRepository trailerRepository;
    @Autowired
    private CompanyRepository companyRepository;

    @Test
    public void testGetAllTrucks() {
        List<TruckEntity> all = truckRepository.findAll();
        for (int i = 1; i <= all.size(); i++) {
            assertEquals(i, all.get(i - 1).getId());
        }
        assertEquals(2, all.size());
    }

    @Test
    public void testGetTruckById() {
        TruckEntity truckEntity = this.truckRepository.findById(1L).get();
        assertEquals("Dodge", truckEntity.getModel());
        assertEquals(1L, truckEntity.getId());
        assertEquals("A0-A2", truckEntity.getTrailer().getSerialNumber());
    }

    @Test
    public void testAddTruckWithGivenModel() {
        TruckEntity entity = new TruckEntity();
        entity.setModel("TestTruck");
        truckRepository.saveAndFlush(entity);
        assertEquals(3, truckRepository.findAll().size());
    }

    @Test
    public void testAddCompanyToGivenTruck() {
        TruckEntity truckEntity = this.truckRepository.findById(2L).get();
        CompanyEntity companyEntity = this.companyRepository.findById(2L).get();
        Set<CompanyEntity> companyEntitySet = new HashSet<>();
        companyEntitySet.add(companyEntity);
        truckEntity.setCompanies(companyEntitySet);
        assertEquals(1, truckEntity.getCompanies().size());
        this.truckRepository.saveAndFlush(truckEntity);
        assertEquals(1, truckEntity.getCompanies().size());
    }

    @Test
    public void testRemoveCompanyFromGivenTruck() {
        TruckEntity truckEntity = this.truckRepository.findById(1L).get();
        assertEquals(1, truckEntity.getCompanies().size());
        CompanyEntity companyEntity = this.companyRepository.findById(1L).get();
        assertEquals(2, companyEntity.getTrucks().size());

        companyEntity.setTrucks(companyEntity.getTrucks().stream().filter(t->!t.getId().equals(truckEntity.getId())).collect(Collectors.toSet()));
        truckEntity.setCompanies(truckEntity.getCompanies().stream().filter(c -> !c.getId().equals(companyEntity.getId())).collect(Collectors.toSet()));

        assertEquals(0, truckEntity.getCompanies().size());
        assertEquals(1, companyEntity.getTrucks().size());
        this.truckRepository.saveAndFlush(truckEntity);
        assertEquals(0, truckEntity.getCompanies().size());
        assertEquals(1, companyEntity.getTrucks().size());
    }

    @Test
    public void testAddTrailerToGivenTruck() {
        TruckEntity truckEntity = this.truckRepository.findById(1L).get();
        TrailerEntity trailerEntity = trailerRepository.findById(3L).get();
        truckEntity.setTrailer(trailerEntity);
        assertEquals(truckEntity.getTrailer().getSerialNumber(), trailerEntity.getSerialNumber());
        truckRepository.save(truckEntity);
        assertEquals(truckEntity.getTrailer().getSerialNumber(), trailerEntity.getSerialNumber());
    }

    @Test
    public void testRemoveTrailerFromGivenTruckShouldReturnNull() {
        TruckEntity truckEntity = this.truckRepository.findById(1L).get();
        truckEntity.setTrailer(null);
        assertNull(truckEntity.getTrailer());
        this.truckRepository.save(truckEntity);
        assertNull(truckEntity.getTrailer());
    }

    @Test
    public void testAddTruckWithGivenModelWithoutAddTrailerShouldReturnNull() {
        TruckEntity entity = new TruckEntity();
        entity.setModel("TestTruck");
        truckRepository.saveAndFlush(entity);
        TrailerEntity expected = truckRepository.findByModel("TestTruck").get().getTrailer();
        assertNull(expected);
    }

    @Test
    public void testEditTruckDoesExist() {
        TruckEntity truckEntity = this.truckRepository.findById(1L).get();
        truckEntity.setModel("newModel");
        truckEntity.setTrailer(this.trailerRepository.findById(1L).get());
        this.truckRepository.saveAndFlush(truckEntity);

        assertEquals("newModel", this.truckRepository.findById(1L).get().getModel());
        assertEquals("A0-A2", this.truckRepository.findById(1L).get().getTrailer().getSerialNumber());
    }

    @Test
    public void testTruckDoesNotExistShouldReturnFalse() {
        Optional<TruckEntity> entity = this.truckRepository.findById(100L);
        assertFalse(entity.isPresent());
    }

    @Test
    public void testDeleteTruck() {
        Optional<TruckEntity> entity = this.truckRepository.findById(1L);

        this.truckRepository.delete(entity.get());

        assertFalse(this.truckRepository.findById(1L).isPresent());
    }
}
