package com.lightsoft.demo;

import com.lightsoft.demo.domain.entities.TrailerEntity;
import com.lightsoft.demo.domain.entities.TruckEntity;
import com.lightsoft.demo.domain.repository.TrailerRepository;
import com.lightsoft.demo.domain.repository.TruckRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class TrailerTest {

    @Autowired
    private TrailerRepository trailerRepository;
    @Autowired
    private TruckRepository truckRepository;

    @Test
    public void testGetAllTrailers() {
        List<TrailerEntity> all = this.trailerRepository.findAll();
        assertEquals(3, all.size());

        for (int i = 1; i <= all.size(); i++) {
            assertEquals(i, all.get(i - 1).getId());
        }
    }

    @Test
    public void testAddTrailer() {
        TrailerEntity trailerEntity = new TrailerEntity();
        trailerEntity.setSerialNumber("some serial number");
        this.trailerRepository.saveAndFlush(trailerEntity);
        assertEquals("some serial number", this.trailerRepository.findById(4L).get().getSerialNumber());
        assertEquals(4, this.trailerRepository.findAll().size());
    }

    @Test
    public void testGetTrailerById() {
        TrailerEntity trailerEntity = this.trailerRepository.findById(1L).get();
        assertEquals("A0-A2", trailerEntity.getSerialNumber());
        assertEquals(1L, trailerEntity.getId());
    }

    @Test
    public void testFindTrailerBySerialNumber() {
        TrailerEntity entity = this.trailerRepository.findById(1L).get();
        assertEquals("A0-A2", entity.getSerialNumber());
        entity = this.trailerRepository.findById(2L).get();
        assertEquals("G3-F3", entity.getSerialNumber());
    }

    @Test
    public void testEditTrailer() {
        TrailerEntity entity = this.trailerRepository.findById(1L).get();
        entity.setSerialNumber("newSerialNumber");
        this.trailerRepository.saveAndFlush(entity);
        assertEquals("newSerialNumber", this.trailerRepository.findById(1L).get().getSerialNumber());
        assertEquals(1, this.trailerRepository.findById(1L).get().getId());
    }

    @Test
    public void testDeleteTrailerDoesIsNotConnectToTruck() {
        TrailerEntity entity = this.trailerRepository.findById(3L).get();
        this.trailerRepository.delete(entity);
        assertEquals(2, this.trailerRepository.findAll().size());
        assertFalse(this.trailerRepository.findById(3L).isPresent());
    }

    @Test(expected = org.springframework.dao.DataIntegrityViolationException.class)
    public void testDeleteTrailerDoesIsConnectToTruckShouldThrowException() {
        TrailerEntity entity = this.trailerRepository.findById(2L).get();
        this.trailerRepository.delete(entity);
        assertFalse(this.trailerRepository.findById(2L).isPresent());
        assertEquals(2, this.trailerRepository.findAll().size());
    }

    @Test
    public void testAddTruckToTrailer(){
        TrailerEntity trailerEntity = this.trailerRepository.findById(1L).get();
        TruckEntity truckEntity = this.truckRepository.findById(1L).get();
        trailerEntity.setTruck(truckEntity);
        assertEquals(trailerEntity.getTruck().getModel(), truckEntity.getTrailer().getTruck().getModel());
        this.trailerRepository.saveAndFlush(trailerEntity);
        assertEquals(trailerEntity.getTruck().getModel(), truckEntity.getTrailer().getTruck().getModel());
    }

    @Test
    public void testRemoveTruckFromTrailer() {
        TrailerEntity trailerEntity = this.trailerRepository.findById(1L).get();
        trailerEntity.setTruck(null);
        assertNull(trailerEntity.getTruck());
        this.trailerRepository.saveAndFlush(trailerEntity);
        assertNull(trailerEntity.getTruck());
    }
}
