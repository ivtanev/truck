package com.lightsoft.demo;

import com.lightsoft.demo.domain.entities.CompanyEntity;
import com.lightsoft.demo.domain.entities.TruckEntity;
import com.lightsoft.demo.domain.repository.CompanyRepository;
import com.lightsoft.demo.domain.repository.TruckRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class CompanyTest {
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private TruckRepository truckRepository;

    @Test
    public void testGetAllCompanies() {
        List<CompanyEntity> all = companyRepository.findAll();
        for (int i = 1; i <= all.size(); i++) {
            assertEquals(i, all.get(i - 1).getId());
        }
        assertEquals(2, all.size());
    }

    @Test
    public void testGetCompanyById() {
        CompanyEntity companyEntity = this.companyRepository.findById(1L).get();
        assertEquals("newCompany", companyEntity.getName());
        assertEquals(1L, companyEntity.getId());
        assertEquals(2, companyEntity.getTrucks().size());
    }

    @Test
    public void testAddCompanyWithGivenName() {
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setName("AnotherCompany");
        companyRepository.saveAndFlush(companyEntity);
        assertEquals(3, companyRepository.findAll().size());
    }

    @Test
    public void testEditCompanyDoesExist() {
        Optional<CompanyEntity> entity = companyRepository.findById(1L);

        if (entity.isPresent()) {
            entity.get().setName("TestCompany");
            companyRepository.saveAndFlush(entity.get());
        }

        String actual = companyRepository.findByName("TestCompany").get().getName();

        assertEquals("TestCompany", actual);
    }

    @Test
    public void testCompanyDoesNotExistShouldReturnFalse() {
        Optional<CompanyEntity> entity = companyRepository.findById(10L);

        assertFalse(entity.isPresent());
    }

    @Test
    public void testDeleteCompanyDoesExist() {
        Optional<CompanyEntity> entity = companyRepository.findById(1L);

        if (entity.isPresent()) {
            companyRepository.delete(entity.get());
        }

        Boolean actual = companyRepository.findByName("newCompany").isPresent();

        assertEquals(false, actual);
    }

    @Test
    public void addTrucksToGivenCompany() {
        CompanyEntity companyEntity = new CompanyEntity();
        companyEntity.setName("testCompany");
        Set<TruckEntity> trucks = new LinkedHashSet<>();
        trucks.add(this.truckRepository.findById(1L).get());
        trucks.add(this.truckRepository.findById(2L).get());
        companyEntity.setTrucks(trucks);
        this.companyRepository.saveAndFlush(companyEntity);
        assertEquals(2, this.companyRepository.findByName("testCompany").get().getTrucks().size());
        Optional<CompanyEntity> company = this.companyRepository.findByName("testCompany");
        boolean expected = company.get().getTrucks().contains(this.truckRepository.findById(1L).get());
        assertTrue(expected);
        expected = company.get().getTrucks().contains(this.truckRepository.findById(2L).get());
        assertTrue(expected);
    }

    @Test
    public void removeTruckFromGivenCompany() {
        CompanyEntity companyEntity = this.companyRepository.findById(1L).get();
        TruckEntity truckEntity = this.truckRepository.findById(2L).get();
        companyEntity.setTrucks(companyEntity.getTrucks().stream()
                .filter(t -> !t.getId().equals(truckEntity.getId()))
                .collect(Collectors.toSet()));
        assertEquals(1, companyEntity.getTrucks().size());
        assertFalse(companyEntity.getTrucks().contains(truckEntity));
        this.companyRepository.save(companyEntity);
        assertEquals(1, this.companyRepository.findById(1L).get().getTrucks().size());
        assertFalse(this.companyRepository.findById(1L).get().getTrucks().contains(truckEntity));
    }

}
