CREATE SEQUENCE public.shipwreck_seq
    INCREMENT 1
    START 4
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

CREATE TABLE IF NOT EXISTS Trailers (
    id bigint auto_increment PRIMARY KEY,
    serial_number varchar(255)
);

CREATE TABLE IF NOT EXISTS Companies (
    id bigint auto_increment PRIMARY KEY,
    name varchar(255)
);

CREATE TABLE IF NOT EXISTS Trucks (
    id bigint auto_increment PRIMARY KEY,
    model varchar(255),
    trailer_id INT(15),
    FOREIGN KEY (trailer_id) REFERENCES Trailers(id)
);


CREATE TABLE IF NOT EXISTS Companies_Trucks(
    company_id INT(15),
    truck_id INT(15),
    FOREIGN KEY (company_id) REFERENCES Companies(id),
    FOREIGN KEY (truck_id) REFERENCES Trucks(id)
);

INSERT INTO Companies (name) VALUES ('newCompany');
INSERT INTO Companies (name) VALUES('Company');

INSERT INTO Trailers (serial_number) VALUES ('A0-A2');
INSERT INTO Trailers (serial_number) VALUES ('G3-F3');
INSERT INTO Trailers (serial_number) VALUES ('R4-TY-E2');

INSERT INTO Trucks (model, trailer_id) VALUES ('Dodge', 1);
INSERT INTO Trucks (model, trailer_id) VALUES('Man', 2);

INSERT INTO Companies_Trucks(company_id, truck_id) VALUES (1,1);
INSERT INTO Companies_Trucks(company_id, truck_id) VALUES (1,2);
INSERT INTO Companies_Trucks(company_id, truck_id) VALUES (2,2);









